#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      kingston
#
# Created:     26/04/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
{
    'name': 'Hotel State Madukaxxxx',
    'version': '1.0',
    'author': 'Maduka Sopulu',
    'description':"""

    This module adds functional refinement to the default Hotel custom Management module on Odoo
    it adds outstanding field, changes state label, calculate outstanding in sale order and also calculates the

Module Features:
================
* Add Check-In Reservation View
* Add Check-Out Reservation View
    """,
    'depends':[
        'sale',
        'hotel_booking',
    ],
    'data':[
        'hotel_change_view.xml'
    ]
}
