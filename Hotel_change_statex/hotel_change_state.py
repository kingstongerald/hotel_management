#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      kingston
#
# Created:     06/05/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from odoo import models, fields, api, _
from odoo.exceptions import except_orm, ValidationError


class ChangesSales(models.Model):
    _inherit = "sale.order"
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'In-Progress'),
        ('sale', 'Check-in'),
        ('done', 'Check-Out'),
        ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')
    outstanding = fields.Float(string='Outstanding', store=True, readonly=True, compute='all_paid', track_visibility='always')

    @api.depends('order_line.advance_p','amount_total')
    def all_paid(self):
        for order in self:
            balan = 0.0
            for line in order.order_line:
                balan += line.advance_p  # makes advance payment increases from zero to anything,
            order.update({
                'outstanding': order.amount_total - balan, # updates the value of outstanding between total amount and advance payment

                })

    @api.constrains('order_line.advance_p')
    def _advance_pay(self):
        for order in order_Line:
            if order.advance_P > order.price_unit:
                raise except_orm(_('Hotel Checkinn Validation'),
                                 _('Bill Should not be great than the Total Amount.'))

    '''@api.depends('order_line.advance_p','amount_total')
    def _balance_all(self):
        """
        Compute the total amounts of the SO.
        """
        for order in self:
            balance = order.amount_total - order.order_line.advance_p

            order.update({

                'balance_total': balance

            })'''

'''
class ChangesSales(models.Model):
    _inherit = "sale.order.line"
    advance_p = fields.Float(string='Advance Payment', default = 0.00, required = False, store=True)

class ChangesHotel(models.Model):
    _inherit = "hotel.folio"


    @api.multi
    def action_confirm(self):
        for order in self.order_id:
            order.state = 'sale'
            order.order_line._action_procurement_create()
            if not order.project_id:
                for line in order.order_line:
                    if line.product_id.invoice_policy == 'cost':
                        order._create_analytic_account()
                        break
        if self.env['ir.values'].get_default('sale.config.settings',
                                             'auto_done_setting'):
            self.order_id.action_done()

    @api.multi
    def action_done(self):
        self.order_id.state = 'done'
'''

'''
class ChangesReservation(models.Model):
    _inherit = "hotel.reservation"
    state = fields.Selection([('draft', 'New'), ('confirm', 'Confirm'),
                              ('cancel', 'Cancel'), ('check-in', 'Check-In'),
                              ('check-out','Check-Out'), ('done', 'Paid')],
                             'State', readonly=True,
                             default=lambda *a: 'draft')
'''
