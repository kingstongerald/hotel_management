#-------------------------------------------------------------------------------
# Name:        module2
# Purpose:
#
# Author:      kingston
#
# Created:     18/04/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
{
    'name': 'Hotel Management Advance Payment',
    'author':'Maduka Sopulu',
    'website':'www.vasconsolutions.com',
    'version':'1.0',
    'description':"""

    This module adds functional refinement to the default Hotel Folio module on$

Module Features:
================
* Add Advance Payment View
* Add Advance Payment View
    """,
    'data':[
        'views/custom_advance_payment.xml',
        'views/account_voucher_report.xml',
        'security/ir.model.access.csv'
    ],
    'depends':[
      'hotel','hotel_reservation','account_voucher'
    ],
    'installable':True,
}



