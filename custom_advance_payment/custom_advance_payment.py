#-------------------------------------------------------------------------------
# Name:        module2
# Purpose:
#
# Author:      kingston
#
# Created:     18/04/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from odoo import models,fields, api, exceptions,_
from datetime import datetime,timedelta

from odoo.exceptions import except_orm, ValidationError


class RegisterPaymentWizardForHotelxxx(models.Model):
    _name = 'register.payment.wizard.for.hotelxxx'
    _description = 'Folio Register Payment'

    @api.model
    def _compute_amount(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        expense_sheet = self.env['hotel.folio'].browse(active_ids)
        return expense_sheet.order_id.amount_total
    @api.model
    def _product_id(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids',[])
        product_id = self.env['hotel.folio'].browse(active_ids)
        return product_id.room_lines[0].product_id.id

    customer_id = fields.Many2one('res.partner', 'Customer', required=True)
    folio_id = fields.Many2one('hotel.folio', 'Folio ID', required=True)
    #payment_method = fields.Many2one('account.journal', 'Payment Method', required=True)
    payment_method = fields.Many2one('account.journal', 'Payment Method', required=True)#default=lambda self: self.env['account.journal'].search([('id', '=', 1)], limit=1))
#
    advance_account = fields.Many2one('account.account', 'Advance Account', required=True)
    date = fields.Datetime('Date', default=fields.Datetime.now)
    product_id = fields.Many2one('product.template','Room/Product',required=True, default=_product_id)

    amount = fields.Float('Paid Amount', required=True, readonly = False)
    amountx = fields.Float('Actual Amount',readonly = True, store = True, default=_compute_amount)
    bal = fields.Float('Balance',store = True, readonly = False)#,compute = "_main_payment", default = 0.00) ###########################################


    @api.depends('amountx','amount')
    def _main_payment(self):
        for x in self:
            main_pay = x.amountx - x.amount
            x.bal = main_pay

    @api.one
    def button_pay(self):
        print 'nice'
        '''context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        expense_sheet = self.env['hotel.folio'].browse(active_ids)###########################################
        expense_sheet.order_id.outstanding = self.bal #* 0'''

        return{'type': 'ir.actions.act_window_close'}



    @api.model
    def create(self, values):
        response = super(RegisterPaymentWizardForHotelxxx, self).create(values)
######################################################

        contexto = dict(self._context or {})
        active_o = contexto.get('active_ids', [])
        expense = self.env['hotel.folio'].browse(active_o)
        zero = 0.00
#######################################################
        # Save the account.voucher leg of the transaction
        journal_obj = self.env['account.journal'].search([('id', '=', values.get('payment_method'))])
        account_voucher_data = {
            'partner_id': values.get('customer_id'),
            'journal_id': values.get('payment_method'),
            'type': 'receipt',
            'state': 'posted',
             'amount': values.get('amount'),
            #'advance_account_id': values.get('advance_account'),
            'account_id': journal_obj.default_debit_account_id.id,
        }
        account_voucher_response = self.env['account.voucher'].create(account_voucher_data)

        print '#####################'
        print account_voucher_response
        print 'Saved account.voucher, about to save into the hotel.folio'

        # Save the hotel.folio.service_lines leg of the transaction
        partner_id = self.env['res.partner'].search([('id','=',values.get('customer_id'))])
        advance_product_id = self.env['product.product'].search([('id','=',values.get('product_id'))])
        folio_obj = self.env['hotel.folio'].search([('id', '=', values.get('folio_id'))])
        sale_order_line_data = {
         'product_id': advance_product_id.id,
         'advance_p': values.get('amount'),#
         'price_unit': 0.00,#-values.get('amount'),###########################################
         'order_id': folio_obj.order_id.id,
         'name': 'Advanced Payment',
         }
        sales_order_line_response = self.env['sale.order.line'].create(sale_order_line_data)

        '''hotel_folio_data = {
         'folio_id': values.get('folio_id'),
         'service_line_id': sales_order_line_response.id,
         'ser_checkin_date': datetime.now() + timedelta(hours=1),
         'ser_checkout_date': datetime.now() + timedelta(hours=2),
        }
        sales_order_line_response = self.env['sale.order.line'].create(sale_order_line_data)
'''
        hotel_folio_data = {
         'folio_id': values.get('folio_id'),
         'service_line_id': sales_order_line_response.id,
         'ser_checkin_date': datetime.now() + timedelta(hours=1),
         'ser_checkout_date': datetime.now() + timedelta(hours=2),
        }


        hotel_service_line_response = self.env['hotel.service.line'].create(hotel_folio_data)
        print hotel_service_line_response

        payment_data = {
            'name': expense.partner_id.name,#values.get('customer_id'),
            'folio_ids':expense.name,#values.get('folio_id'),
            'amount':values.get('amount'),
            'date':values.get('date'),
            'balance':expense.outstanding #* 0
            }

        payment_model = self.env['hotel.paymen.customs'].create(payment_data)

        #payment_id = payment_data.id


        return response



class CustomAdvancePayment(models.Model):
    _inherit = 'hotel.folio'

    @api.multi
    def create_advance_payment(self,values):
        """Get the advance product details from product.template."""
        customer = self.env['res.partner'].search([('id','=',values.get('partner_id'))])
        advance_product_id = self.env['product.template'].\
            search([('name', 'ilike', 'advance payment'),('company_id','=',customer.company_id.id)])

        folio_obj = self.env['hotel.folio'].search([('id', '=', values.get('folio_id'))])
        room_line_obj = folio_obj.room_lines.product_id.id



        print "+++++===+++++++++++++++++++++++++++"
        print advance_product_id.id
        print "+++++===+++++++++++++++++++++++++++"
        return {
            'name': 'Register Payment',
            'view_type': 'form',
            "view_mode": 'form',
            'res_model': 'register.payment.wizard.for.hotelxxx',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {
                'default_customer_id': self.partner_id.id,
                #'default_advance_account':self.partner_id.property_account_receivable_id.id,
                'default_folio_id': self.id,
                #'default_product_id': self.room_lines.product_id.id,#room_line_obj,#advance_product_id.id,
                'default_amount':self.order_id.outstanding

            },
           }

    @api.multi
    def checkout_complete(self):

        amount_total = self.order_id.amount_total
        zero = 0.0
        #$%$$$$$$$$%%%%%%%%%%%%%%%%%%%%%%%%%$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        folio_line = self.room_lines[0].product_id.id
        room_res = self.env['hotel.room'].search([('product_id', '=', folio_line)])
        #$%$$$$$$$$%%%%%%%%%%%%%%%%%%%%%%%%%$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        #if self.order_id.outstanding > zero:
            #raise except_orm(_('Hotel Checkinn Validation'),
                                 #_('Guest must clear Bills before Checkout'))
        #else:
        self.order_id.state = "done"
        room_res.status = 'available'
        room_res.isroom = False #$%$$$$$$$$%%%%%%%%%%%%%%%%%%%%%%%%%$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

    @api.multi
    def print_offer_letter_account(self):
        self.ensure_one()
        #switch offer letter type to pdf
        report = self.env['ir.actions.report.xml'].search([('report_name', '=', 'hotel_new_printer.report_receipt_document_hotel')], limit=1)
        if report:
            report.write({'report_type': 'qweb-pdf'})

        return self.env['report'].get_action(self.id, 'hotel_new_printer.report_receipt_document_hotel')

class HotelReservationHidex(models.Model):
    _inherit = 'hotel.reservation'
    state = fields.Selection([('draft', 'New'), ('confirm', 'Confirm'),
                              ('cancel', 'Cancel'), ('check-in', 'Check-In'), ('folio', 'Folio'),
                              ('check-out','Check-Out'), ('done', 'Paid')],
                             'State', readonly=True,
                             default=lambda *a: 'draft')



class ChangesSalesX(models.Model):
    _inherit = "sale.order"
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'In-Progress'),
        ('sale', 'Check-in'),
        ('done', 'Check-Out'),
        ('cancel', 'Cancelled'),
        ('close', 'Closed'),
        ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')

class CustomerPayments(models.Model):
    _inherit = 'account.voucher'












