# -*- coding: utf-8 -*-
{
    'name' : 'Hotel Restaurant Reportd',
    'version' : '1.0',
    'summary': 'Custom report for the restaurant sales',
    'sequence': 16,
    'category': 'Restaurant',
    'description': """
Custom Sales Report
=====================================
Resaurant report that will generate a pdf report for a sales person for specific time duration.
    """,


    'depends' : ['hotel_restaurant'],
    'data': [
        'wizard/salesperson_report_view.xml',
        'views/sales_report_report.xml',
        'views/report_salesperson.xml'
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
