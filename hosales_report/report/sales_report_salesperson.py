# -*- coding: utf-8 -*-

import time
from odoo import fields, api, models, _
from dateutil.parser import parse
#from odoo.exceptons import ValidationError
#from openerp.exceptions import UserError orders
from odoo.exceptions import except_orm, ValidationError

class ReportSalesperson(models.AbstractModel):
    _name = 'report.hosales_report.report_hosalesperson'

    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        sales_records = []
        orders = self.env['hotel.restaurant.order'].search([('create_uid', '=', docs.salesperson_id.id)])
        if docs.date_from and docs.date_to:
            for order in orders:
                if parse(docs.date_from) <= parse(order.create_date) and parse(docs.date_to) >= parse(order.create_date):
                    sales_records.append(order);

        else:
            #raise ("Please enter duration")
            raise ValidationError(_('Please enter duration'))

           #pass

        '''if docs.date_from:
            for order in orders:
                if parse(docs.date_from) == parse(order.create_date):
                    sales_records.append(order);'''

        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'orders': sales_records
        }
        return self.env['report'].render('hosales_report.report_hosalesperson', docargs)




