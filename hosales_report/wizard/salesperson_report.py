# -*- coding: utf-8 -*-

from odoo import api, fields, models

from dateutil.relativedelta import relativedelta
import datetime
import time

class SalespersonWizardho(models.TransientModel):
    _name = "salesperson.wizardho"
    _description = "Salesperson wizard"

    salesperson_id = fields.Many2one('res.users', string='Salesperson', required=True)#, default=self.create_uid)
    date_from = fields.Datetime(string='Date', default=fields.Datetime.now)
    date_to = fields.Datetime(string='End Date',default=fields.Datetime.now)#,default=lambda *a: (datetime.datetime.now() + relativedelta(hours=9)).strftime('%Y-%m-%d %H:%M:%S'))
    #default= fields.Datetime.now)

    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['salesperson_id', 'date_from'])[0]
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['salesperson_id', 'date_from'])[0])
        return self.env['report'].get_action(self, 'hosales_report.report_hosalesperson', data=data)