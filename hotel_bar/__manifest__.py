# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Hotel Bar and Restaurant',
    'version': '10.0.1.0.0',
    'author': 'Maduka Sopulu',
    'description':"""Hotel Restaurant application to seperate Bar and restaurant module fro m hotel Management Base""",
    'category': 'Hotel',

    'depends': ['hotel'],
    'data': [
        'hotel_bar_view.xml',
        #'security/ir.model.access.csv',
        'security/security_groups.xml'
    ],

    'installable': True,
    'auto_install': False,
}
