{
'name':'Hotel Complementaries',
'author':'Maduka Sopulu Chris',
'description':"""The module helps hotel users to add customers who has
                been offered free services on first checkin""",
'summary':'Registers hotel guest offer complimentary services',
'depends':['hotel'],
'data':[
        'security/security_groups.xml',
        'views/hotel_complement_views.xml',

        ],
'auto_install':False,
'installable':True,

}