from odoo import fields, models, api, _
import datetime

import time

import urllib2
from odoo.exceptions import ValidationError
from odoo.osv import expression
from odoo.tools import misc, DEFAULT_SERVER_DATETIME_FORMAT

from decimal import Decimal
from dateutil.relativedelta import relativedelta
# defining class of hotel complement
class Hotel_complement(models.Model):
    _name = "hotel.complement"
    #_order = "asc"
    room_no =fields.Many2one('product.product', 'Room No.')
    customer_name = fields.Many2one('res.partner', 'Guest')
    date_checkin = fields.Datetime("Checkin Date")
    ######################

    folio_no = fields.Many2one('hotel.folio', 'Folio Number')
    compliment_line =fields.Many2many('hotel.service.line', string ='Complementary Lines', readonly=True)

# defining the class of hotel folio inheritance

class Hotel_Folio_complement(models.Model):
    _inherit ="hotel.folio"
    checkin_date = fields.Datetime('Check-In', required=True, readonly=True,states={'draft': [('readonly', False)]},default=lambda *a: (datetime.datetime.now() + relativedelta(minutes=58)).strftime('%Y-%m-%d %H:%M:%S'))
    service_comp_lines = fields.One2many('hotel.service.line', 'folio_id',
                                    readonly=True,
                                    states={'draft': [('readonly', False)],
                                            'sent': [('readonly', False)],
                                            'sale': [('readonly', False)]},
                                    help="Hotel services detail provide to"
                                    "customer")



    @api.multi
    def action_confirm(self):
        for order in self.order_id:
            order.state = 'sale'
            order.order_line._action_procurement_create()
            if not order.project_id:
                for line in order.order_line:
                    if line.product_id.invoice_policy == 'cost':
                        order._create_analytic_account()
                        break
        if self.env['ir.values'].get_default('sale.config.settings',
                                             'auto_done_setting'):
            self.order_id.action_done()
        self.order_id.action_invoice_create()
        self.write({'no_ids': self.order_id.id, 'so_no':self.order_id.name})
        self.add_complement_order()


    @api.multi
    def add_complement_order(self):
        for rec in self:
            val = {}
            val['folio_no']=self.id
            val['date_checkin']=self.date_order
            val['customer_name']=self.partner_id.id

            comp_service = self.env['hotel.complement']
            comp_id = comp_service.create(val)
            comp_service_search = self.env['hotel.complement'].search([('id','=',comp_id.id)])
                #comp_service_search.write({'compliment_line':[(0,0,(val2))]})
            comp_service_search.compliment_line = self.service_comp_lines

