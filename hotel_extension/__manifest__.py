# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Hotel Extension M',
    'version': '10.0.1.0.0',
    'author': 'Maduka Sopulu',
    'description':"""Extends hotel guest stay""",
    'category': 'Hotel',

    'depends': ['hotel'],
    'data': [
        'hotel_extension_view.xml',
        'security/ir.model.access.csv'
    ],

    'installable': True,
    'auto_install': False,
}
