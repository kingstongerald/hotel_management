from odoo import api, models, fields, _
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import time
from odoo.exceptions import except_orm, ValidationError


class HotelFolioStayExtension(models.Model):
    _name = 'hotel.folio.stay.extension'

    nos_of_days = fields.Selection([('1', '1 day'), ('2', '2 days'),
                                    ('3', '3 days'), ('4', '4 days'),
                                    ('5', '5 days'), ('6', '6 days'),
                                    ('7', '7 days'), ('30', '30 days')], 'Extend by', default=lambda *a: '1', required=True)
    checkin_date = fields.Datetime('Check-In Date', help="This is the date that marks the start of an extension. The "
                                                         "date is set to be the current check-out date", required=True)
    folio_id = fields.Many2one('hotel.folio', 'Folio ID', required=True)
    #$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    advance = fields.Float('Advance', default = 0.00)
    discount = fields.Float('Discount', default = 0.00)

    #$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    @api.one
    def button_extend(self):
        print 'nice'
        return{'type': 'ir.actions.act_window_close'}

    @api.model
    def create(self, values):
        response = super(HotelFolioStayExtension, self).create(values)

        folio_obj = self.env['hotel.folio'].search([('id', '=', values.get('folio_id'))])
        room_line_obj_date = folio_obj.room_lines[-1]#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        room_line_obj = folio_obj.room_lines[0] #$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        checkin_date = datetime.strptime(room_line_obj_date.checkout_date, '%Y-%m-%d %H:%M:%S')
        checkout_date = checkin_date + timedelta(days=int(values.get("nos_of_days")))

        folio_lines_val = {
            'checkin_date': checkin_date,
            'checkout_date': checkout_date,
            'product_id': room_line_obj.product_id.id,
            'name': folio_obj.room_lines[0].name + " [check-in: "+ checkin_date.strftime('%d/%m/%Y')+", check-out: "+
                    checkout_date.strftime('%d/%m/%Y') + "]",
            'product_uom': room_line_obj.product_uom.id,
            'price_unit': room_line_obj.price_unit,
            'product_uom_qty': values.get('nos_of_days'),
            'advance_p': values.get('advance'), #$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            'discount': values.get('discount'),#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            'folio_id': folio_obj.id,
            'is_reserved': True
        }
        res_obj = self.env['hotel.room'].search([('product_id', '=', room_line_obj.product_id.id)])
        folio_room_line_obj = self.env['folio.room.line']

        h_room_obj = self.env['hotel.room']

        room_obj = h_room_obj.search([('name','=',room_line_obj.product_id.name)])
        room_obj.write({'isroom':False})

        vals = {'room_id':room_obj.id,
        	'check_in':checkin_date,
        	'check_out':checkout_date,
        	'folio_id':folio_obj.id,}

        #if res_obj.status == 'occupied':
            #pass#raise except_orm(_('Hotel Checkinn Validation'),_('The room is currently not available'))
        #else:
        self.env['hotel.folio.line'].create(folio_lines_val)
        folio_room_line_obj.create(vals)

        folio_obj.checkout_date = checkout_date #this is used to update the folio to the new checkout date.
        folio_obj.checkin_date = checkin_date # Used to update the folio to the new checkinn date #$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

            #res_obj = self.env['hotel.room'].search([('product_id', '=', room_line_obj.product_id.id)])
        res_obj.status = 'occupied'
        res_obj.isroom = False


        return response



class HotelFolioStayExtensionModified(models.Model):
    _inherit = 'hotel.folio'

    '''@api.model
    def check_expired_reservations(self):
        """
        This method is used to automatically extend a reservation that has the checkout date as yesterday
        but is still active.
        :return:
        """
        print "###############################################"
        print "Calling -- check_expired_reservations()"
        print "###############################################"

        now_str = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        now_date = datetime.strptime(now_str, DEFAULT_SERVER_DATETIME_FORMAT)
        for folio in self.search([]):

            checkout_date = (datetime.strptime(folio.checkout_date, DEFAULT_SERVER_DATETIME_FORMAT))
            difference = relativedelta(now_date, checkout_date)
            extension_days = difference.days

            if extension_days >= 1 and not folio.hotel_invoice_id.id:
                room_line_obj = folio.room_lines[-1]
                checkin_date = datetime.strptime(room_line_obj.checkout_date, '%Y-%m-%d %H:%M:%S')
                checkout_date = checkin_date + timedelta(days=extension_days + 1)

                #checkin_date_temp =  datetime.strptime(room_line_obj.checkin_date, '%Y-%m-%d %H:%M:%S')
                #checkin_date = checkin_date_temp if datetime.now > checkin_date_temp else datetime.now

                folio_lines_val = {
                    'checkin_date': checkin_date,
                    'checkout_date': checkout_date,
                    'product_id': room_line_obj.product_id.id,
                    'name': folio.room_lines[0].name + " [check-in: "+ checkin_date.strftime('%d/%m/%Y')+
                            ", check-out: "+ checkout_date.strftime('%d/%m/%Y') + "]",
                    'product_uom': room_line_obj.product_uom.id,
                    'price_unit': room_line_obj.price_unit,
                    'product_uom_qty': extension_days,
                    'folio_id': folio.id,
                    'is_reserved': True
                }
                self.env['hotel.folio.line'].create(folio_lines_val)
                folio.checkout_date = checkout_date #this is used to update the folio object and set the checkout_date

                res_obj = self.env['hotel.room'].search([('product_id', '=', room_line_obj.product_id.id)])
                res_obj.status = 'occupied'
                res_obj.isroom = False
        return True'''


    @api.multi
    def extend_hotel_stay(self):
        """
        This method is used to extend the guest stay in the hotel from the Hotel Folio module
        :return: Object of hotel.folio.stay.extension
        """
        return {
              'name': 'Extend Hotel Stay',
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'hotel.folio.stay.extension',
              'type': 'ir.actions.act_window',
              'target': 'new',
              'context': {
                  'default_folio_id': self.id,
                  'default_checkin_date': self.checkout_date
              },
        }



