#-------------------------------------------------------------------------------
# Name:        module2
# Purpose:
#
# Author:      kingston
#
# Created:     06/04/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
{
'name':'Hotel Print Invoice S',
'description':"""Inventory reporting module""",
'summary':'Can generate report of records',
'author':'Maduka Sopulu',
'depends':['hotel'],
'data':['guest_print.xml']
}