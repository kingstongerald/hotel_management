{
    'name': 'Hotel HouseKeeping Security',
    'version': '10.0.1.0.0',
    'author': 'Maduka Sopulu',
    'description':"""Hotel Security and access rights""",
    'category': 'Hotel',

    'depends': ['hotel', 'hotel_housekeeping','hotel_security_access'],
    'data': [
        'security/ir.model.access.csv',
    ],

    'installable': True,
    'auto_install': False,
}
