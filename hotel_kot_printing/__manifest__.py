#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      kingston
#
# Created:     20/04/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
{
    'name': 'Hotel Restaurant Order/ KOT Print',
    'version': '10.0.1.0.0',
    'author': 'Maduka Sopulu',
    'category': 'Hotel',


    'depends': ['hotel','hotel_restaurant'],
    'data': [
        'reports/hotel_restaurant_print.xml',
        'reports/hotel_printing_report.xml',

        'hotel_kot_print_view.xml',
    ],


    'installable': True,
    'auto_install': False,
}
