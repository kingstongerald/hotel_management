import time
from odoo import models, fields, api, _
from odoo.exceptions import except_orm, ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


import os
from time import sleep

class KotPrinter(models.Model):
    _inherit = 'hotel.restaurant.kitchen.order.tickets'
    @api.multi
    def printing_kot(self):
        return self.env['report'].get_action(self.id, 'hotel_kot_printing.hotel_kot_printer_template')


class RestaurantOrder(models.Model):
    _inherit = 'hotel.restaurant.order'
    @api.multi
    def generate_kot_update(self):
        """
        This method update record for hotel restaurant order list.
        ----------------------------------------------------------
        @param self: The object pointer
        @return: update record set for hotel restaurant order list.
        """
        order_tickets_obj = self.env['hotel.restaurant.kitchen.order.tickets']
        rest_order_list_obj = self.env['hotel.restaurant.order.list']
        for order in self:
            table_ids = [x.id for x in order.table_no]
            line_data = {
                'orderno': order.order_no,
                'kot_date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                'room_no': order.room_no.name,
                'w_name': order.waiter_name.name,
                'tableno': [(6, 0, table_ids)],
                }
            kot_obj = order_tickets_obj.browse(self.kitchen_id)
            kot_obj.write(line_data)
            for order_line in order.order_list:
                if order_line.id not in order.rest_item_id.ids:
                    kot_data1 = order_tickets_obj.create(line_data)
                    self.kitchen_id = kot_data1.id
                    o_line = {
                        'kot_order_list': kot_data1.id,
                        'name': order_line.name.id,
                        'item_qty': order_line.item_qty,
                        'item_rate': order_line.item_rate
                    }
                    self.rest_item_id = [(4, order_line.id)]
                    rest_order_list_obj.create(o_line)
        return True

    '''@api.multi
    def printing_receipt(self):
        return self.env['report'].get_action(self.id, 'hotel_kot_printing.report_receipt_document_hotel_restaurant')'''
    '''@api.multi
    def fileopen(self):
        path = 'C:\\Program Files (x86)\\Odoo 10.0\\server\\odoo\\addons\\hotel_kot_printing\\__init__.py'
        text = 'Osgetologotolo'
        oread = open(path,'w')
        oread.write(text)
        oread.close
        return oread'''

    @api.multi
    def printing_receipt(self):
        #self.fileopen()

        return self.env['report'].get_action(self.id, 'hotel_kot_printing.report_receipt_document_hotel_restaurant')

        #return True
    '''def fileopen(self):
        path = 'C:\\Program Files (x86)\\Odoo 10.0\\server\\odoo\\addons\\hotel_kot_printing\\__init__.py'
        text = 'Osgetologotolo'
        oread = open(path,'w')
        oread.write(text)
        oread.close
        return oread'''


