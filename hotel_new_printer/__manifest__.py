#-------------------------------------------------------------------------------
# Name:        module2
# Purpose:
#
# Author:      kingston
#
# Created:     06/04/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
{
'name':'Hotel Receipt Printer',
'description':"""Used to print receipt for hotel module""",
'summary':'Hotel Receipt for guest',
'author':'Maduka Sopulu',
'depends':['hotel'],
'data':['hotel_printer_button.xml'],
'installable': True,
    'auto_install': False,
}