{
'name': 'Hotel Pos User Report',
'author':'Maduka Sopulu',
'description':"""Hotel Pos Module is used to print all the reports made by hotel sales person from
the POS end""",

'data':[
        'views/report_view.xml',
        'wizard/hotel_pos_user_report_view.xml',
        ],
'depends': ['hotel'],
'auto_install':False,
'installable':True,
}