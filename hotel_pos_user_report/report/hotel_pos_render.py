# -*- coding: utf-8 -*-
import time
from odoo import api, models
from dateutil.parser import parse
#from openerp.exceptions import UserError

class ReportSalesperson(models.AbstractModel):
    _name = 'report.hotel_pos_user_report.hotel_pos_user_rep_id'

    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        sales_records = []
        orders = self.env['pos.order'].search([('user_id', '=', docs.sales_person.id)])
        if docs.date_from and docs.date_to:
            for order in orders:
                if parse(docs.date_from) <= parse(order.date_order) and parse(docs.date_to) >= parse(order.date_order):
                    sales_records.append(order);
        else:
           #  raise UserError("Please enter duration")
           pass

        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'orders': sales_records
        }
        return self.env['report'].render('hotel_pos_user_report.hotel_pos_user_rep_id', docargs)



