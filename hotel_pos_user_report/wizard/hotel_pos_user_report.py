from odoo import fields, models, api, _
class Model_Print(models.TransientModel):
    _name = 'hotel.sale.report'
    sales_person = fields.Many2one('res.users', string='Salesperson', required=True)
    date_from = fields.Datetime('Start Date')
    date_to = fields.Datetime('Date To')

    @api.multi
    def check_rep(self):
        data = {}

        data['form'] = self.read(['sales_person','date_from', 'date_to'])[0]

        return self.printer_receipt(data)

    def printer_receipt(self, data):
        data['form'].update(self.read(['sales_person', 'date_from', 'date_to'])[0])
        return self.env['report'].get_action(self.id, 'hotel_pos_user_report.report_pos_user_sales_id', data=data)