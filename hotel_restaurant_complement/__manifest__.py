#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      kingston
#
# Created:     20/04/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
{
    'name': 'Hotel Restaurant Complements',
    'version': '10.0.1.0.0',
    'author': 'Maduka Sopulu',
    'description':"""Gets the list of all those guest given complementary breakfast""",
    'category': 'hotel',

    'depends': ['hotel_restaurant'],
    'data': [
        'hotel_res_comp_view.xml',
        'security/security_group.xml',
        'security/ir.model.access.csv',
    ],


    'installable': True,
    'auto_install': False,
}
