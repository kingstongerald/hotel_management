#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Maduka Sopulu
#
# Created:     20/04/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.exceptions import except_orm, ValidationError


from datetime import datetime, timedelta
import time


class RestaurantComplement(models.Model):
    _name = 'restaurant.complement'

    @api.onchange('room_no')
    def get_folio_id(self):
        if self.room_no:
            hotel_room_obj = self.env['hotel.room'].search([('id','=',self.room_no.id)])
            folio_room_obj = self.env['folio.room.line'].search([('room_id.name','=', self.room_no.name),('status','=','sale')])
            if not folio_room_obj:
                self.folio_room_obj = None
                self.partner_id = None
            else:
                self.folio_id = folio_room_obj[-1].folio_id
                self.partner_id = folio_room_obj[-1].folio_id.partner_id
                self.partner_invoice_id = folio_room_obj[-1].folio_id.partner_id
                self.partner_shipping_id=folio_room_obj[-1].folio_id.partner_id
    api.depends('folio_id')
    def get_duration_days(self):
        for x in self:
            get_folio_dur = x.folio_id.duration
            x.duration_stay=get_folio_dur

    partner_id = fields.Many2one('res.partner','Guest Name', )
    date = fields.Datetime(string='Date',default=fields.Date.context_today, required=True, copy=False)
    waitress = fields.Many2one('hr.employee', string = 'Waitress')
    folio_id= fields.Many2one('hotel.folio', 'Folio No')
    duration_stay =fields.Integer('Duration of Stay', compute='get_duration_days')
    room_no = fields.Many2one('product.product', string='Room No', size=64, domain=[('isroom','=', True)])
    comment_text=fields.Text('Reasons')
    state = fields.Selection([('draft', 'Draft'),
                                ('done', 'Done')
                              ], string='Status')
    is_guest = fields.Boolean('Is guest')
    total_amount = fields.Float('Total Worth', compute="worth_rev")
    partner_invoice_id = fields.Many2one('res.partner', string='Invoice Address', readonly=True, required=False,
                                         states={'draft': [('readonly', False)]},
                                         help="Invoice address for current sales order.")
    partner_shipping_id = fields.Many2one('res.partner', string='Delivery Address', readonly=True, required=False,
                                          states={'draft': [('readonly', False)]},
                                          help="Delivery address for current sales order.")
    order_lines = fields.One2many('restaurant.comp.line', 'restaurant_com_obj', required=1, ondelete='cascade')

    @api.depends('order_lines.sub_amount')
    def worth_rev(self):
        for order in self:
            total = 0.0
            for line in order.order_lines:
                total += line.sub_amount  # makes advance payment increases from zero to anything,
            order.update({
                'total_amount': total # updates the value of outstanding between total amount and advance payment

                })


    @api.multi
    def print_button(self):
        pass
    @api.multi
    def confirm_breakfast(self):
        self.state = "done"
    @api.multi
    def cancel_breakfast(self):
        self.state = "draft"
        for rec in order_lines:
            rec.unlink()


class RestaurantComplementLine(models.Model):
    _name = 'restaurant.comp.line'

    @api.depends('price_item', 'qty')
    def get_amount(self):
        for obj in self:
            total = obj.price_item*obj.qty
            obj.sub_amount = total

    @api.onchange('product_id')
    def change_description(self):
        get_description = " -Breakfast- " +str(self.product_id.name)
        self.description = get_description

    product_id = fields.Many2one('product.product', string='Product/Item', required=1)
    qty = fields.Integer(string='Qty of items', required=True, default=1)
    description = fields.Text(string='Description')
    price_item = fields.Float(string='Price', required=1, default=2000.00)
    sub_amount = fields.Float(compute='get_amount', string='Amount')
    restaurant_com_obj = fields.Many2one('restaurant.complement', invisible=1)
