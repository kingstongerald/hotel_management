{
    'name': 'Hotel Security Rights M',
    'version': '10.0.1.0.0',
    'author': 'Maduka Sopulu',
    'description':"""Hotel Security and access rights""",
    'category': 'Hotel',

    'depends': ['hotel', 'hotel_restaurant', 'hotel_housekeeping'],
    'data': [
        'security/security_groups.xml',
        'security/ir.model.access.csv',


    ],

    'installable': True,
    'auto_install': False,
}
